#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent): 
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_serial(new QSerialPort(this))

{
    ui->setupUi(this);

    //connect(m_serial, &QSerialPort::errorOccurred, this, &MainWindow::handleError);
    //connect(m_serial, &QSerialPort::readyRead, this, &MainWindow::readData);
    //connect(m_console, &Console::getData, this, &MainWindow::writeData);

    openSerialPort();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_spinBox_valueChanged(int arg1)
{

    SerialStringCan1 = QString("can1 %1").arg(arg1);
    qDebug() << SerialStringCan1;

}

void MainWindow::on_spinBox_2_valueChanged(int arg1)
{
    SerialStringCan2 = QString("can2 %1").arg(arg1);
    qDebug() << SerialStringCan2;
}


void MainWindow::openSerialPort()
{
    //const SettingsDialog::Settings p = m_settings->settings();
    m_serial->setPortName("ttyACM0");
    m_serial->setBaudRate(9600);
    m_serial->setDataBits(QSerialPort::Data8);
    m_serial->setParity(QSerialPort::NoParity);
    m_serial->setStopBits(QSerialPort::OneStop);
    m_serial->setFlowControl(QSerialPort::NoFlowControl);
    if (m_serial->open(QIODevice::ReadWrite)) {
      //showStatusMessage(tr("Connected to ttyACM0"));
    }
}

void MainWindow::writeData(const QByteArray &data)
{
    m_serial->write(data);
}
void MainWindow::on_pushButton_clicked()
{
  buffer.clear();
  buffer = buffer.append(SerialStringCan1);
  qDebug() << buffer;
  writeData(buffer);
}



void MainWindow::on_pushButton_2_clicked()
{
    buffer.clear();
    buffer = buffer.append(SerialStringCan2);
    qDebug() << buffer;
    writeData(buffer);
}
