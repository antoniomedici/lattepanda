#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSerialPort>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_spinBox_valueChanged(int arg1);
    void openSerialPort();
    //void closeSerialPort();
    //void about();
    void writeData(const QByteArray &data);
    //void readData();
    //void handleError(QSerialPort::SerialPortError error);

    void on_pushButton_clicked();

    void on_spinBox_2_valueChanged(int arg1);

    void on_pushButton_2_clicked();

private:
    void showStatusMessage(const QString &message);
    Ui::MainWindow *ui;
    QSerialPort *m_serial = nullptr;
    QString SerialStringCan1 = "can1 0";
    QString SerialStringCan2 = "can2 0";
    QByteArray buffer;
};
#endif // MAINWINDOW_H
