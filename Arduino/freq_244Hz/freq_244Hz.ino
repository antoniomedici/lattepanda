
String stringa;
String Canale;
String Valore;

int ledPin1 = 9;
int ledPin2 = 10; 

int percentualeIntensita = 0;
int analogValue = 0;

void setup() {
  Serial.begin(9600);
  pinMode(ledPin1, OUTPUT);
  pinMode(ledPin2, OUTPUT);
  setPwmFrequency(ledPin1, 256); // Cambio la frequenza della PWM da 977Hz a 244Hz
  setPwmFrequency(ledPin2, 256); // Cambio la frequenza della PWM da 977Hz a 244Hz
}

void loop() {

  if (Serial.available())
  {
      stringa += Serial.readString(); 
  
      Canale = stringa.substring(0, 4);
      Valore = stringa.substring(5, 8);
      stringa = "";
   
    percentualeIntensita = Valore.toInt();
    if (percentualeIntensita == 0)
       analogValue = 0;
    else if (percentualeIntensita >= 100)
       analogValue = 255;
    else
       analogValue = (percentualeIntensita * 255)/100;

      Serial.println(Valore);

    if (Canale == "can1")      
      analogWrite(ledPin1, analogValue);
    else if (Canale == "can2")
        analogWrite(ledPin2, analogValue);

  }

}

void setPwmFrequency(int pin, int divisor) //funzione per modificare la frequenza del PWM
{
  byte mode;
  if (pin == 5 || pin == 6 || pin == 9 || pin == 10) //se il pin è uno di questi
  {
    switch (divisor) //in base al divisore scelto viene dato un valore esadecimale a "mode"
    {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 64: mode = 0x03; break;
      case 256: mode = 0x04; break;
      case 1024: mode = 0x05; break;
      default: return;
    }
    if (pin == 5 || pin == 6) //se il pin è il 5 o il 6 cambio il valore del registro TCCR0B
    {
      TCCR0B = (TCCR0B & 0b11111000) | mode;
    }
    else                    //altrimenti cambio il valore del registro TCCR1B
    {
      TCCR1B = (TCCR1B & 0b11111000) | mode;
    }
  }
  else if (pin == 3 || pin == 11) //se il pin è il 3 o 11 cambio il valore del registro TCCR2B
  {
    switch (divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 32: mode = 0x03; break;
      case 64: mode = 0x04; break;
      case 128: mode = 0x05; break;
      case 256: mode = 0x06; break;
      case 1024: mode = 0x07; break;
      default: return;
    }
    TCCR0B = (TCCR0B & 0b11111000) | mode;
  }
}
